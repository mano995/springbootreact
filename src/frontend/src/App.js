import {useState, useEffect} from 'react'
import {deleteStudent, getAllStudents, updateStudent} from "./client";
import {
    Layout, Menu, Breadcrumb, Table, Spin,
    Empty, Button, Badge, Tag,
    message
} from 'antd';
import {
    DesktopOutlined,
    PieChartOutlined,
    FileOutlined,
    TeamOutlined,
    UserOutlined,
    LoadingOutlined,
    DownloadOutlined,
    PlusOutlined
} from '@ant-design/icons';
import StudentDrawerForm from "./StudentDrawerForm";
import './App.css';
import Avatar from "antd/es/avatar/avatar";
import ThePopconfirm from "./ThePopconfirm";
import {errorNotification} from "./Notification";

const {Header, Content, Footer, Sider} = Layout;
const {SubMenu} = Menu;
const TheAvatar = ({name}) => {

    if (name.trim().length === 0) {
        return <Avatar icon={<UserOutlined/>}/>
    }

    const spilt = name.trim().split(" ");

    if (spilt.length === 1) {
        return <Avatar>{name.charAt(0).toUpperCase()}</Avatar>
    }

    return <Avatar>{spilt[0].charAt(0).toUpperCase() +spilt[1].charAt(0).toUpperCase()}</Avatar>
}

function App() {
    const [students, setStudents] = useState([]);
    const [collapsed, setCollapsed] = useState(false);
    const [fetching, setFetching] = useState(true);
    const [showDrawer, setShowDrawer] = useState(false);



    const columns = [
        {
            title: '',
            dataIndex: 'avatar',
            key: 'avatar',
            render:(text,student) => <TheAvatar name={student.name}/>
        },
        {
            title: 'Id',
            dataIndex: 'id',
            key: 'id'
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        }
        , {
            title: 'Email',
            dataIndex: 'email',
            key: 'email'
        },
        {
            title: 'Gender',
            dataIndex: 'gender',
            key: 'gender'
        },
        {
            title: 'Actions',
            dataIndex: 'actions',
            key: 'actions',
            render:(text,student) => <>
                <ThePopconfirm modifiyStudent={deleteStudent} student={student} fetchStudents={fetchStudents} isDeleting={true}/>
                <ThePopconfirm modifiyStudent={updateStudent} student={student} fetchStudents={fetchStudents} isDeleting={false} setShowStudentDrawer={setShowDrawer}   />
                                     </>

        }
    ];
    const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;


    const fetchStudents = () =>
        getAllStudents()
            .then(res => res.json())
            .then(data => {
                setStudents(data);
                setFetching(false);
            }).catch(err => {
                console.log(err.response)
                err.response.json().then(res => {
                    console.log(res);
                    err.response.json().then(()=>{
                        errorNotification(
                            "there was an issue",
                            `${res.message} [status Code: ${res.status}] `
                        );
                        }
                    );
                })
        }).finally( () => {setFetching(false)})

    useEffect(() => {
        console.log("compi is mounteds");
        fetchStudents();
    }, []);

    const renderStudents = () => {

        if (fetching) {
            return <Spin indicator={antIcon}/>
        }

        if (students.length <= 0) {
            return <>
                <StudentDrawerForm
                    showDrawer={showDrawer}
                    setShowDrawer={setShowDrawer}
                    fetchStudents={fetchStudents}

                />
                <Button type="primary" shape="round" icon={<PlusOutlined />} onClick={() => setShowDrawer(!showDrawer)} size="small">
                    Add Student
                </Button>
                <Empty/>
                </>
        }

        return   <>
                 <StudentDrawerForm
                     title="Create new student"
                     showDrawer={showDrawer}
                     setShowDrawer={setShowDrawer}
                     fetchStudents={fetchStudents}

                    />
                <Table dataSource={students}
                      columns={columns}
                      bordered
                      title={() => <>
                          <Tag > Number of students</Tag>
                          <Badge  count={students.length} className="site-badge-count-4"  />
                          <br/><br/>
                          <Button type="primary" shape="round" icon={<PlusOutlined />} onClick={() => setShowDrawer(!showDrawer)} size="small">
                              Add Student
                          </Button>
                        </>}
                      pagination={{pageSize: 50}}
                      scroll={{y: 1000}}
                      rowKey={(student) => student.id}/>
                </>;
    }

    return <Layout style={{minHeight: '100vh'}}>
        <Sider collapsible collapsed={collapsed}
               onCollapse={setCollapsed}>
            <div className="logo"/>
            <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
                <Menu.Item key="1" icon={<PieChartOutlined/>}>
                    Option 1
                </Menu.Item>
                <Menu.Item key="2" icon={<DesktopOutlined/>}>
                    Option 2
                </Menu.Item>
                <SubMenu key="sub1" icon={<UserOutlined/>} title="User">
                    <Menu.Item key="3">Tom</Menu.Item>
                    <Menu.Item key="4">Bill</Menu.Item>
                    <Menu.Item key="5">Alex</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<TeamOutlined/>} title="Team">
                    <Menu.Item key="6">Team 1</Menu.Item>
                    <Menu.Item key="8">Team 2</Menu.Item>
                </SubMenu>
                <Menu.Item key="9" icon={<FileOutlined/>}>
                    Files
                </Menu.Item>
            </Menu>
        </Sider>
        <Layout className="site-layout">
            <Header className="site-layout-background" style={{padding: 0}}/>
            <Content style={{margin: '0 16px'}}>
                <Breadcrumb style={{margin: '16px 0'}}>
                    <Breadcrumb.Item>User</Breadcrumb.Item>
                    <Breadcrumb.Item>Bill</Breadcrumb.Item>
                </Breadcrumb>
                <div className="site-layout-background" style={{padding: 24, minHeight: 360}}>
                    {renderStudents()}
                </div>
            </Content>
            <Footer style={{textAlign: 'center'}}>Amino Samino V3 ©2023 Created by Amin Amer</Footer>
        </Layout>
    </Layout>;
}

export default App;
