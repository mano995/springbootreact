import { Button, message, Popconfirm } from 'antd';
import {errorNotification, successNotification} from "./Notification";



function ThePopconfirm({modifiyStudent,student,fetchStudents,isDeleting,setShowStudentDrawer})  {
    const confirm = (e) => {
        console.log(student);

        if (isDeleting) {
            modifiyStudent(student.id)
                .then(() => fetchStudents());
           // message.success( isDeleting ? `student ${student.name} deleted!`: `student ${student.name} updated!`);
            successNotification(isDeleting ? `student ${student.name} deleted!`: `student ${student.name} updated!`)
        }else {
            setShowStudentDrawer(true)

        }


    };
    const cancel = (e) => {
        console.log(e);

        errorNotification(isDeleting ? `student was not ${student.name} deleted!`: `student ${student.name} was not updated!`)
    };
    return  <Popconfirm
        title={ isDeleting ? `Are you sure to delete student ${student.name}?` : `Are you sure to update student ${student.name}?`}
        description=""
        onConfirm={confirm}
        onCancel={cancel}
        okText="Yes"
        cancelText="No"
    >
        {isDeleting && <Button danger  >Delete</Button> }
        {!isDeleting && <Button >Edit</Button> }
    </Popconfirm>;
}
export default ThePopconfirm