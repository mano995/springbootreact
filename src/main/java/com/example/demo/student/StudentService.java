package com.example.demo.student;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class StudentService {
    private final StudentRepo studentRepo;

    public List<Student> getAllStudent()  {
        return studentRepo.findAll();
    }

    public void addStudent(Student student) {
        // check if email is taken
        List<Long> s = studentRepo.getStudentEmail(student.getEmail());
        if (s.size() > 0) {
            throw new IllegalStateException("Email already in use!");
        }
        studentRepo.save(student);

    }

    public void deleteStudent(Long studentId) {
        // check if email is taken
        studentRepo.deleteById(studentId);
    }

    public Optional<Student> getStudent(Long studentId) {
        return studentRepo.findById(studentId);
    }
    public void updateStudent(Student student) {
        //
        studentRepo.updateStudent(student.getId(),student.getName(),student.getEmail(),student.getGender());
    }

}
