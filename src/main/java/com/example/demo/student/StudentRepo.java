package com.example.demo.student;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Repository
public interface StudentRepo  extends JpaRepository<Student, Long> {

    @Modifying
    @Transactional
    @Query("update Student s set s.name=:name, s.email =:email,s.gender=:gender where s.id = :id ")
void updateStudent(@Param(value = "id") long id ,@Param(value = "name") String name ,@Param(value = "email") String email ,@Param(value = "gender") Gender gender);

    @Query("select distinct a.id from Student a where a.email = :email")
    List<Long> getStudentEmail(String email);

}
